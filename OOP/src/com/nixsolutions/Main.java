package com.nixsolutions;

import figure.Circle;
import figure.Figure;
import figure.Square;
import figure.Triangle;

import java.util.Random;

public class Main {
    private static Random random = new Random();

    public static void main(String[] args) {

        System.out.print("------- Изначальные фигуры -------");
        Figure[] figures = new Figure[10];

        for (int i = 0; i < 10; i++) {
            int figureType = random.nextInt(3);
            double x = createRandom();
            double y = createRandom();

            switch (figureType) {
                case 0:
                    double r = createRandom();
                    figures[i] = new Circle(x, y, r);
                    break;
                case 1:
                    double size = createRandom();
                    figures[i] = new Square(x, y, size);
                    break;
                case 2:
                    double a = createRandom();
                    double b = random.nextDouble() + a;
                    double c = random.nextDouble() + b;
                    figures[i] = new Triangle(x, y, a, b, c);
                    break;
            }
            printFigure(figures[i]);
        }

        System.out.print("\n\n------- Фигуры после изменения размера -------");
        for (int i = 0; i < 10; i++) {
            double k = random.nextDouble();
            figures[i].changeSize(k);

            printFigure(figures[i]);
            System.out.print(" (коэффициент размера: " + round(k) + ")");
        }

        System.out.print("\n\n------- Сортировка фигур с новым размером -------");
        bubbleSort(figures);
        for (int i = 0; i < 10; i++) {
            printFigure(figures[i]);
        }
    }

    // Сортировка фигур по площади
    private static void bubbleSort(Figure[] figures) {
        for (int i = figures.length - 1; i >= 0; i--) {

            for (int j = 0; j < i; j++) {
                if (figures[j].calcArea() > figures[j + 1].calcArea()) {
                    Figure temp = figures[j];
                    figures[j] = figures[j + 1];
                    figures[j + 1] = temp;
                }
            }
        }
    }

    // вывод фигуры в консоль
    private static void printFigure(Figure figure) {
        String typeName = "";
        if (figure instanceof Circle) {
            typeName = "Круг";
        } else if (figure instanceof Square) {
            typeName = "Квадрат";
        } else if (figure instanceof Triangle) {
            typeName = "Треугольник";
        }
        double roundedArea = round(figure.calcArea());
        System.out.print("\n" + typeName + " с площадью " + roundedArea);
    }

    private static double createRandom() {
        return (random.nextDouble() + 1) * 10;
    }

    private static double round(double param) {
        return (double) Math.round(param * 100) / 100;
    }
}
