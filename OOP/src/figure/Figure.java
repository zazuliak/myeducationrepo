package figure;

/**
 * Created by Anna Zazuliak on 11/23/2016.
 */
public abstract class Figure {
    protected double x, y; //одна координата (x,y) для каждой фигуры

    protected Figure(double x, double y) {
        this.x = x;
        this.y = y;

    }

    public void move(double xDelta, double yDelta) {
        x = x + xDelta;
        y = y + yDelta;
    }

    public abstract double calcArea();

    public abstract void changeSize(double k);
}
