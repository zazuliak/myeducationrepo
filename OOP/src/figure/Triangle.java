package figure;

/**
 * Created by Anna Zazuliak on 11/23/2016.
 */
public class Triangle extends Figure {

    private double a;
    private double b;
    private double c;

    public Triangle(double x, double y, double a, double b, double c) {
        super(x, y);
        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
            throw new IllegalArgumentException("Из сгенерированных сторон нельзя построить треугольник.");
        }
    }

    @Override
    public double calcArea() {
        double p = calcHalfPerimeter();
        double area = Math.sqrt(p * (p - this.a) * (p - this.b) * (p - this.c));
        return area;
    }

    @Override
    public void changeSize(double k) {
        this.a = this.a * k;
        this.b = this.b * k;
        this.c = this.c * k;
    }

    private double calcHalfPerimeter() {
        return (this.a + this.b + this.c) / 2;
    }
}
