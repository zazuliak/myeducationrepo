package figure;

/**
 * Created by Anna Zazuliak on 11/23/2016.
 */
public class Circle extends Figure {
    private double r;

    public Circle(double x, double y, double r) {
        super(x, y);
        this.r = r;
    }

    @Override
    public double calcArea() {
        return Math.PI * r * r;
    }

    @Override
    public void changeSize(double k) {
        this.r = this.r * k;
    }
}
