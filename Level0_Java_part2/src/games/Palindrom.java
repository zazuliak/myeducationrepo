package games;

/**
 * Created by Anna Zazuliak on 11/22/2016.
 */
public class Palindrom {

    public static boolean checkWord(String someWord) {
        char[] chars = someWord.toCharArray();
        char[] arrayWord = new char[chars.length];

        for (int i = chars.length - 1; i >= 0; i--) {
            arrayWord[chars.length - i - 1] = chars[i];
        }
        String palindrom = new String(arrayWord);
        int result = someWord.compareToIgnoreCase(palindrom);
        if (result == 0) {
            return true;
        } else return false;
    }

    public static boolean checkPhrase(String somePhrase) {
        String str1 = somePhrase.replaceAll("\\s", "");
        char[] chars = str1.toCharArray();
        char[] arrayPhrase = new char[chars.length];
        for (int i = chars.length - 1; i >= 0; i--) {
            arrayPhrase[chars.length - i - 1] = chars[i];
        }
        String palindrom = new String(arrayPhrase);
        return str1.equalsIgnoreCase(palindrom);
    }
}
