package games;

/**
 * Created by Anna Zazuliak on 11/22/2016.
 */
public class Snail {
    public static int[][] calculateSnail(int size) {
        int[][] matrix = new int[size][size];
        int row = 0;
        int col = 0;
        int dx = 1;
        int dy = 0;
        int dirChanges = 0;
        int visits = size;

        for (int i = size * size - 1; i >= 0; i--) {
            matrix[row][col] = i + 1;
            if (--visits == 0) {
                visits = size * (dirChanges % 2) +
                        size * ((dirChanges + 1) % 2) -
                        (dirChanges / 2 - 1) - 2;
                int temp = dx;
                dx = -dy;
                dy = temp;
                dirChanges++;
            }
            col += dx;
            row += dy;
        }

        return matrix;
    }
}
