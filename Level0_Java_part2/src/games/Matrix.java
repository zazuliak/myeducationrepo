package games;

/**
 * Created by Anna Zazuliak on 11/20/2016.
 */
public class Matrix {
    public static int[][] createMatrix(int matrixSize) { // построение матрицы
        int[][] matrix = new int[matrixSize][matrixSize];
        int a = 1;

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                matrix[i][j] = a;

                if (a == 9) {
                    a = 1;
                } else {
                    a++;
                }
            }
        }
        return matrix;
    }
}
