package calculator;

/**
 * Created by Anna Zazuliak on 11/21/2016.
 */
public class Arithmetic {
    public static long arrayMultiplication(int[] array) {
        long resMult = 1;

        for (int element : array) {
            resMult = resMult * element;
        }

        return resMult;
    }

    public static double power(double base, double exponent) {
        double resPower = Math.pow(base, exponent); // TODO: обработка переполнения
        return resPower;

    }

    public static double division(double divisor, double dividend) {
        double resDivision;
        resDivision = divisor / dividend; // TODO: обработка деления на ноль
        return resDivision;
    }

    public static double root(double num) {
        double resRoot = Math.sqrt(num);
        return resRoot;
    }
}
