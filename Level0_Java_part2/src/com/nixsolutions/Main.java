package com.nixsolutions;

import calculator.Arithmetic;
import games.Matrix;
import games.Palindrom;
import games.Snail;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("1) Формирование двумерного массива.");
            System.out.print("\nВведите число от 1 до 9 для задания размерности матрицы: ");
            int matrixSize;

            try {
                matrixSize = sc.nextInt();//считываем целое число
            } catch (InputMismatchException e) {
                System.out.println("Ошибка. Введены недопустимые символы.");
                sc = new Scanner(System.in);
                continue;
            }

            if (matrixSize < 1 || matrixSize > 9) {
                System.out.println("Ошибка. Некорректное число.");
                continue;
            }

            int[][] resultMatrix = Matrix.createMatrix(matrixSize);
            PrintMatrix(resultMatrix);

            System.out.println("Введите 'x', чтобы выйти или любой другой символ, чтобы повторить: ");
            boolean shouldExit = sc.next().equals("x");

            if (shouldExit) {
                break;
            }
        } //закончили работу с матрицами

        // ввод данных для умножения
        System.out.println("2) Переумножение чисел из массива.");
        System.out.println("Введите, пожалуйста, размерность массива:");
        int sizeArray = sc.nextInt();
        int[] array = new int[sizeArray];
        for (int i = 0; i < sizeArray; i++) {
            System.out.println("Введите, пожалуйста," + i + "-й элемент массива:");
            array[i] = sc.nextInt();
        }
        long resMult = Arithmetic.arrayMultiplication(array);
        System.out.println("Результат: " + resMult);

        //ввод данных для возведения в степень
        System.out.println("3) Возведение числа в степень.");
        System.out.println("Введите, пожалуйста, число:");
        double base = sc.nextDouble();
        System.out.println("Введите, пожалуйста, степень для числа:");
        double exponent = sc.nextDouble();
        double resPower = Arithmetic.power(base, exponent);
        System.out.println("Результат: " + resPower);

        // ввод данных для деления
        System.out.println("4) Деление двух чисел.");
        System.out.println("Введите, пожалуйста, делимое число:");
        double divisor = sc.nextDouble();
        System.out.println("Введите, пожалуйста, число делитель:");
        double dividend = sc.nextDouble();
        double resDivision = Arithmetic.division(divisor, dividend);
        System.out.println("Результат: " + resDivision);

        // ввод данных для вычисления квадратного корня
        System.out.println("5) Извлечение квадратного корня из числа.");
        System.out.println("Введите, пожалуйста, число:");
        double root = sc.nextDouble();
        double resRoot = Arithmetic.root(root);
        System.out.println("Результат: " + resRoot);

        //улитка
        int sizeSnail;
        while (true) {
            System.out.println("6) Улитка (заполнение матрицы по спирали).");
            System.out.println("Введите, пожалуйста, размерность квадратной матрицы (число больше 3): ");
            sizeSnail = sc.nextInt();

            if (sizeSnail <= 3) {
                System.out.println("Ошибка. Некорректное число.");
                continue;
            } else break;
        }
        int[][] resultMatrix = Snail.calculateSnail(sizeSnail);
        PrintMatrix(resultMatrix);

        //ввод слова - палиндром
        System.out.println("7) Слово - палиндром.");
        System.out.println("Введите, пожалуйста, слово без пробелов для определения палиндрома:");
        String someWord = sc.next();
        boolean resWordPalindrom = Palindrom.checkWord(someWord);
        if (resWordPalindrom) {
            System.out.println("Результат: слово является палиндромом.");
        } else System.out.println("Результат: введенное слово не является палиндромом.");

        //ввод фразы - палиндром
        System.out.println("7) Фраза - палиндром.");
        System.out.println("Введите, пожалуйста, фразу для определения палиндрома:");
        sc.nextLine();
        String somePhrase = sc.nextLine();
        boolean resPhrasePalindrom = Palindrom.checkPhrase(somePhrase);
        if (resPhrasePalindrom) {
            System.out.println("Результат: фраза является палиндромом.");
        } else System.out.println("Результат: введенная фраза не является палиндромом.");
    }

    // вывод матрицы на экран
    private static void PrintMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}


