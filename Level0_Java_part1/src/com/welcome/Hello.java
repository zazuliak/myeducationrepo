package com.welcome;

/**
 * Created by Anna Zazuliak on 11/19/2016.
 */
public class Hello {
    private String userName;

    public void setupName(String name) {
        userName = name;
    }

    public void welcome() {
        System.out.println("Hello, " + userName + "!");
    }

    public void bye() {
        System.out.println("Bye-bye, " + userName + "!");
    }
}
