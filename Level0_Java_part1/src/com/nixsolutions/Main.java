package com.nixsolutions;

import com.welcome.Hello;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.print("Введите имя и нажмите Enter: ");
        Scanner sc = new Scanner(System.in);
        String nameFromConsole = sc.nextLine();
        Hello hello = new Hello();
        hello.setupName(nameFromConsole);
        hello.welcome();
        System.out.println("Hello, world! \n");
        hello.bye();
    }
}
