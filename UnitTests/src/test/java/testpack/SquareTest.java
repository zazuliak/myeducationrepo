package testpack;

import figure.Square;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anna Zazuliak on 1/3/2017.
 */
public class SquareTest {
    //Для всех тестов используется подход Arrange-Act-Assert.

    // Проверяем, что отрицательные значения сторон недопустимы
    // Тест обнаружил потенциальный баг
    @Test(expected = IllegalArgumentException.class)
    public void constructor_WithNegativeSize_ThrowsIllegalArgumentException() throws Exception {
        Square square = new Square(1.5, 1.5, -5.75);
    }

    // Проверяем, что площадь считается корректно
    @Test
    public void calcArea_WithDoubleSize_ReturnsCorrectArea() throws Exception {
        final double areaWithSize5_75 = 33.0625;
        Square square = new Square(1.5, 1.5, 5.75);

        double actualArea = square.calcArea();

        Assert.assertEquals("Площадь рассчитана некорректно.", areaWithSize5_75, actualArea, 0);
    }

    // Проверяем, что длины сторон квадрата изменились через подсчет площади(т.к. сторона квадрата private)
    @Test
    public void changeSize_WithCoefficient4_IncreasesSquareArea() throws Exception {
        final double areaWithSize24 = 576.0;
        Square square = new Square(1.5, 1.5, 6);

        square.changeSize(4);

        double areaWithNewSize = square.calcArea();
        Assert.assertTrue("Размер квадрата неизменён.", areaWithSize24 == areaWithNewSize);
    }

    // Проверяем, что отрицательный коэффициент недопустим
    // Тест обнаружил потенциальный баг
    @Test(expected = IllegalArgumentException.class)
    public void changeSize_WithNegativeCoefficient_ThrowsIllegalArgumentException() throws Exception {
        Square square = new Square(1.5, 1.5, 6);

        square.changeSize(-4.5);
    }
}
