package testpack;

import figure.Circle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anna Zazuliak on 1/3/2017.
 */
public class CircleTest {
    //Для всех тестов используется подход Arrange-Act-Assert.

    // Проверяем, что отрицательный радиус недопустим
    // Тест обнаружил потенциальный баг
    @Test(expected = IllegalArgumentException.class)
    public void constructor_WithNegativeRadius_ThrowsIllegalArgumentException() throws Exception {
        //Arrange
        Circle circle = new Circle(1, 1, -3);
    }

    // Проверяем, что площадь считается корректно
    @Test
    public void calcArea_WithRadius3_ReturnsCorrectArea() throws Exception {
        //Arrange
        final double areaWithRadius3 = 28.274333882308137;
        Circle circle = new Circle(1, 1, 3);

        //Act
        double actualArea = circle.calcArea();

        //Assert
        Assert.assertEquals("Площадь рассчитана некорректно.", areaWithRadius3, actualArea, 0);
    }

    // Проверяем, что радиус круга изменился через подсчет площади(т.к. радиус private)
    @Test
    public void changeSize_WithCoefficient2_IncreasesCircleArea() throws Exception {
        final double areaWithRadius6 = 113.09733552923255658456;
        Circle circle = new Circle(1, 1, 3);

        circle.changeSize(2);

        double areaWithNewRadius = circle.calcArea();
        Assert.assertTrue("Размер круга неизменён.", areaWithRadius6 == areaWithNewRadius);
    }

    // Проверяем, что отрицательный коэффициент недопустим
    // Тест обнаружил потенциальный баг
    @Test(expected = IllegalArgumentException.class)
    public void changeSize_WithNegativeCoefficient_ThrowsIllegalArgumentException() throws Exception {
        Circle circle = new Circle(1, 1, 3);

        circle.changeSize(-2);
    }
}
