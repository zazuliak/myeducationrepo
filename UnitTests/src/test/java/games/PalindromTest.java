package games;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anna Zazuliak on 1/14/2017.
 */
public class PalindromTest {
    // Позитивный тест для слова
    @Test
    public void checkWord_ForPalindrom_ReturnsTrue() throws Exception {
        String palindrom = "топот";

        boolean isPalindrom = Palindrom.checkWord(palindrom);

        Assert.assertTrue(isPalindrom);
    }

    // Негативный тест для слова
    @Test
    public void checkWord_ForNotPalindrom_ReturnsFalse() throws Exception {
        String notPalindrom = "топор";

        boolean isPalindrom = Palindrom.checkWord(notPalindrom);

        Assert.assertFalse(isPalindrom);
    }

    // Проверка, что checkWord нечувствительный к регистру
    @Test
    public void checkWord_IsCaseInsensitive_ReturnsTrue() throws Exception {
        String palindrom = "тОПоТ";

        boolean isPalindrom = Palindrom.checkWord(palindrom);

        Assert.assertTrue(isPalindrom);
    }

    // Позитивный тест для фразы
    @Test
    public void checkPhrase_WithPhrasePalindrom_ReturnsTrue() throws Exception {
        String palindrom = "нажал кабан на баклажан";

        boolean isPalindrom = Palindrom.checkPhrase(palindrom);

        Assert.assertTrue(isPalindrom);
    }

    // Негативный тест для фразы
    @Test
    public void checkPhrase_ForNotPalindromPhrase_ReturnsFalse() throws Exception {
        String palindrom = "Hello, world!";

        boolean isPalindrom = Palindrom.checkPhrase(palindrom);

        Assert.assertFalse(isPalindrom);
    }

    // Проверка фразы с разным регистром букв
    @Test
    public void checkPhrase_IsCaseInsensitive_ReturnsTrue() throws Exception {
        String palindrom = "Нажал кабан на бАКЛажан";

        boolean isPalindrom = Palindrom.checkPhrase(palindrom);

        Assert.assertTrue(isPalindrom);
    }


}