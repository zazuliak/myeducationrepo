package games;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anna Zazuliak on 1/14/2017.
 */
public class SnailTest {
    // Проверяем, что самое большое число в массиве равно его размерности в квадрате
    @Test
    public void calculateSnail_WithSizeN_ReturnsMaxValueNPow2() throws Exception {
        final int expectedMaxValue = 25; // размер массива в квадрате

        int[][] actualMatrix = Snail.calculateSnail(5);

        int actualMaxValue = actualMatrix[0][0];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < i; j++) {
                if (actualMatrix[i][j] > actualMaxValue) {
                    actualMaxValue = actualMatrix[i][j];
                }
            }
        }

        Assert.assertEquals(expectedMaxValue, actualMaxValue);
    }

    // Проверяем построение квадратной матрицы с заполнением чисел по спирали
    @Test
    public void calculateSnail_WithSizeN_ReturnsMatrixSnailWithSizeN() throws Exception {
        int[][] expectedMatrix = {
                {25, 24, 23, 22, 21},
                {10, 9, 8, 7, 20},
                {11, 2, 1, 6, 19},
                {12, 3, 4, 5, 18},
                {13, 14, 15, 16, 17}};

        int[][] actualMatrix = Snail.calculateSnail(5);

        Assert.assertArrayEquals(expectedMatrix, actualMatrix);
    }

    // Проверяем, что отрицательное значение размерности матрицы недопустимо
    @Test(expected = NegativeArraySizeException.class)
    public void calculateSnail_WithNegativeSize_ThrowsNegativeArraySizeException() throws Exception {
        int[][] actualMatrix = Snail.calculateSnail(-1);
    }

    // Проверяем возможность создания матрицы нулевой длины
    @Test
    public void calculateSnail_WithZeroSize_ReturnsMatrixSnailWithSizeN() throws Exception {
        int[][] actualMatrix = Snail.calculateSnail(0);

        Assert.assertEquals(0, actualMatrix.length);
    }
}





