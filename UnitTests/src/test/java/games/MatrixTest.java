package games;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anna Zazuliak on 1/14/2017.
 */
public class MatrixTest {
    // Проверяем, построение квадратной матрицы (кол-во колонок = кол-ву столбцов = переданому параметру)
    @Test
    public void createMatrix_WithSizeN_ReturnsSquareMatrixOfSizeN() throws Exception {
        final int matrixSize = 5;

        int[][] actualMatrix = Matrix.createMatrix(matrixSize);

        AssertSquareMatrix(matrixSize,actualMatrix);
    }

    // Проверяем построение матрицы со значениями от 1 до 9
    @Test
    public void createMatrix_WithSizeN_ReturnsSquareMatrixOfSizeNWithValuesFrom1To9() throws Exception {
        int[][] expectedMatrix = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 1},
                {2, 3, 4, 5, 6},
                {7, 8, 9, 1, 2},
                {3, 4, 5, 6, 7}};

        int[][] actualMatrix = Matrix.createMatrix(5);

        Assert.assertArrayEquals(expectedMatrix, actualMatrix);
    }

    // Проверяем, что отрицательное значение размерности матрицы недопустимо
    @Test(expected = NegativeArraySizeException.class)
    public void createMatrix_WithNegativeSize_ThrowsNegativeArraySizeException() throws Exception {
        int[][] actualMatrix = Matrix.createMatrix(-1);
    }

    // Assert для проверки квадратной матрицы (кол-во колонок = кол-ву столбцов = ожидаемому размеру)
    private static void AssertSquareMatrix(int expectedSize, int[][] actualMatrix) {
        Assert.assertEquals(expectedSize, actualMatrix.length);

        for (int i = 0; i < actualMatrix.length; i++) {
            Assert.assertEquals(expectedSize, actualMatrix[i].length);
        }
    }
}