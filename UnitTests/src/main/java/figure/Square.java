package figure;

/**
 * Created by Anna Zazuliak on 11/23/2016.
 */
public class Square extends Figure {

    private double size;

    public Square(double x, double y, double size) {
        super(x, y);
        this.size = size;
    }

    @Override
    public double calcArea() {
        return this.size * this.size;
    }

    @Override
    public void changeSize(double k) {
        this.size = this.size * k;
    }
}
