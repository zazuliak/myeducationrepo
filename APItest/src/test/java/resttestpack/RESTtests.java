package resttestpack;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Anna Zazuliak on 2/5/2017.
 */
public class RESTtests {
    private String url;
    private HttpClient client;

    @Before
    public void initializeSteps() {
        client = HttpClientBuilder.create().build();
        url = "http://apilayer.net/api/live";
    }

    @Test
    public void checkAllNecessaryResponseKeys() throws Exception {
        url += "?" + "access_key=f208413b43c382be2f83281665d23bc0&currencies=EUR";
        HttpGet get = new HttpGet(url);

        HttpResponse response = client.execute(get);

        JSONObject jsonObj = new JSONObject(getResponseBody(response));
        Assert.assertTrue("Ключ \"success\" не представлен в ответе", jsonObj.has("success"));
        Assert.assertTrue("Ключ \"terms\" не представлен в ответе", jsonObj.has("terms"));
        Assert.assertTrue("Ключ \"privacy\" не представлен в ответе", jsonObj.has("privacy"));
        Assert.assertTrue("Ключ \"timestamp\" не представлен в ответе", jsonObj.has("timestamp"));
        Assert.assertTrue("Ключ \"source\" не представлен в ответе", jsonObj.has("source"));
        Assert.assertTrue("Ключ \"quotes\" не представлен в ответе", jsonObj.has("quotes"));

    }

    @Test
    public void checkQuotesForSpecifiedCurrency() throws Exception {
        url += "?" + "access_key=f208413b43c382be2f83281665d23bc0&currencies=EUR,UAH,RUB";
        HttpGet get = new HttpGet(url);

        HttpResponse response = client.execute(get);

        JSONObject responseJson = new JSONObject(getResponseBody(response));
        JSONObject quotesJson = responseJson.getJSONObject("quotes");
        Set<String> quotesKeys = quotesJson.keySet();
        String[] expectedQuotes = {"USDEUR", "USDUAH", "USDRUB"};
        Assert.assertTrue("Ключ \"quotes\" не содержит значения \"USDEUR\", \"USDUAH\", \"USDRUB\"", quotesKeys.containsAll(Arrays.asList(expectedQuotes)));
        Assert.assertEquals("Ключ \"quotes\" содержит не 3 значения", 3, quotesKeys.size());
    }

    @Test
    //Тест не проходит, потому что "quotes" содержит 169 комбинаций, а не 170.
    public void checkQuotesListWithoutCurrencies() throws Exception {
        url += "?" + "access_key=f208413b43c382be2f83281665d23bc0";
        HttpGet get = new HttpGet(url);

        HttpResponse response = client.execute(get);

        JSONObject responseJson = new JSONObject(getResponseBody(response));
        JSONObject quotesJson = responseJson.getJSONObject("quotes");
        Set<String> quotesKeys = quotesJson.keySet();
        Assert.assertEquals("Ключ \"quotes\" не содержит 170 комбинаций", 170, quotesKeys.size());
    }

    @Test
    public void checkValueUSDEUR() throws Exception {
        url += "?" + "access_key=f208413b43c382be2f83281665d23bc0&currencies=EUR,UAH";
        HttpGet get = new HttpGet(url);

        HttpResponse response = client.execute(get);

        JSONObject responseJson = new JSONObject(getResponseBody(response));
        JSONObject quotesJson = responseJson.getJSONObject("quotes");
        Double USDEUR = quotesJson.getDouble("USDEUR");
        Assert.assertTrue("Значение курса USDEUR не находится в пределах [0.9;099]", USDEUR >= 0.9 && USDEUR <= 0.99);
    }

    @Test
    public void checkAccessRestrictionWithSourceUAH() throws Exception {
        url += "?" + "access_key=f208413b43c382be2f83281665d23bc0&source=UAH";
        HttpGet get = new HttpGet(url);

        HttpResponse response = client.execute(get);

        JSONObject responseJson = new JSONObject(getResponseBody(response));
        JSONObject errorJson = responseJson.getJSONObject("error");
        int errorCode = errorJson.getInt("code");
        String info = errorJson.getString("info");
        Assert.assertEquals("Код ошибки не 105", 105, errorCode);
        String expectedMessage = "Access Restricted - Your current Subscription Plan does not support Source Currency Switching.";
        Assert.assertEquals("Полученное сообщение отличается от ожидаемого", expectedMessage, info);
    }


    private String getResponseBody(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        String result = "", line = "";
        while ((line = rd.readLine()) != null) {
            result += line;
        }

        return result;
    }
}
