package soaptestpack;

import net.yandex.speller.services.spellservice.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by Anna Zazuliak on 1/29/2017.
 */
public class SOAPtests {

    public CheckTextRequest request;
    public SpellService spell;
    public SpellServiceSoap port;

    @Before
    public void initializeTests() {
        request = new CheckTextRequest();
        spell = new SpellService();
        port = spell.getSpellServiceSoap();
    }

    @Test
    public void checkText_WithMistake_ReturnsErrorCount() throws Exception {
        request.setLang("EN");
        request.setText("Heello dearr frriend");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Кол-во ошибок посчитано неверно", checkTextResponse.getSpellResult().getError().size() == 3);
    }

    @Test
    public void checkText_WithMistake_ReturnsPosAndRowOfMistake() throws Exception {
        request.setLang("EN");
        request.setText("Hello dearr friend");
        CheckTextResponse checkTextResponse = port.checkText(request);
        SpellError actualError = checkTextResponse.getSpellResult().getError().get(0);
        Assert.assertTrue("Параметр pos некорректен", actualError.getPos() == 6);
        Assert.assertTrue("Параметр row некорректен", actualError.getRow() == 0);
    }

    @Test
    public void checkText_WithMistakeAndNextLine_ReturnsPosAndRowOfMistake() throws Exception {
        request.setLang("EN");
        request.setText("Hello\ndearr\nfriend");
        CheckTextResponse checkTextResponse = port.checkText(request);
        SpellError actualError = checkTextResponse.getSpellResult().getError().get(0);
        Assert.assertTrue("Параметр pos некорректен", actualError.getPos() == 6);
        Assert.assertTrue("Параметр row некорректен", actualError.getRow() == 1);
    }

    @Test
    public void checkText_WithMistake_ReturnsCorrectWord() throws Exception {
        request.setLang("RU");
        request.setText("Пьеса была сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);
        SpellError actualError = checkTextResponse.getSpellResult().getError().get(0);
        Assert.assertEquals("Ошибка не обнаружена", "сиграна", actualError.getWord());
        Assert.assertTrue("Кол-во подсказок не равно 1", actualError.getS().size() == 1);
        Assert.assertEquals("Предложенное слово некорректно", "сыграна", actualError.getS().get(0));
    }


    @Test
    public void checkText_WithMistake_ReturnsMultipleSuggestions() throws Exception {
        request.setLang("RU");
        request.setText("сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);
        SpellError actualError = checkTextResponse.getSpellResult().getError().get(0);
        Assert.assertEquals("Ошибка не обнаружена", "сиграна", actualError.getWord());
        Assert.assertTrue("Кол-во подсказок не равно 5", actualError.getS().size() == 5);
        Assert.assertTrue("Предложенное слово не найдено", actualError.getS().contains("сыграна"));
    }

    //Проверка автоматического определения языка через нахождение подсказок для слова с ошибкой.
    @Test
    public void checkLanguageDetectionForRU() throws Exception {
        request.setText("приввет");
        CheckTextResponse checkTextResponse = port.checkText(request);
        SpellError actualError = checkTextResponse.getSpellResult().getError().get(0);
        Assert.assertEquals("Ошибка не обнаружена", "приввет", actualError.getWord());
        Assert.assertTrue("Кол-во подсказок не равно 1", actualError.getS().size() == 1);
        Assert.assertTrue("Предложенное слово не найдено", actualError.getS().contains("привет"));
    }
}